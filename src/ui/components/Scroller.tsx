import React, { useState } from "react";
import { Box, useInput } from "ink";

type Props<T> = {
  items: T[];
  count?: number;
  children: (items: T[]) => React.ReactNode;
  active?: boolean;
};

function Scroller<T>({ items, count = 5, active = true, children }: Props<T>) {
  // cursor at -1 means tailing and shows at most last `count` items.
  const [cursor, setCursor] = useState(-1);

  useInput((_input, key) => {
    if (!key || !active) return;

    if (key.upArrow) {
      if (cursor === -1) {
        setCursor(items.length - 1);
      } else {
        setCursor(Math.max(0, cursor - 1));
      }
      return;
    }

    if (key.downArrow) {
      if (cursor === -1) return;

      if (cursor + (count >>> 1) >= items.length) {
        setCursor(-1);
      } else {
        setCursor(cursor + 1);
      }
      return;
    }
  });

  return (
    <Box flexDirection="column">
      {children(visibleItems(items, cursor, count))}
    </Box>
  );
}

// Return at most `count` items to display.
function visibleItems<T>(items: T[], cursor: number, count: number) {
  if (cursor === -1) return items.slice(-count);

  cursor = Math.min(Math.max(0, cursor), items.length - 1);
  const start = getStartIndex(cursor, count, items.length);
  return items.slice(start, start + count);
}

function getStartIndex(cursor: number, size: number, itemsCount: number) {
  if (itemsCount <= size) return 0;

  const offset = size >>> 1;
  if (cursor + offset >= itemsCount) return itemsCount - size;
  return Math.max(0, cursor - offset);
}

export default Scroller;
