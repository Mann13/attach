import chalk from "chalk";
import meow from "meow";

import Attach from "./index";

(async () => {
  const cli = meow(
    chalk`
{bold USAGE}
  $ qualified-attach

{bold OPTIONS}
  -h, --help         {dim show help}
  -t, --token={underline token}  {dim token provided by web IDE}
  -v, --version      {dim show version}
`,
    {
      flags: {
        token: {
          type: "string",
          alias: "t",
        },
      },
    }
  );
  const attach = new Attach();
  await attach.run(cli.flags);
})();
