export interface File {
  path: string;
  contents: string;
  directory?: boolean;
  readonly?: boolean;
}

export type FileMap = { [path: string]: string };

export type FilesDiff = {
  added: { [path: string]: string };
  deleted: { [path: string]: undefined };
  updated: { [path: string]: string };
};

// TODO any options set by clients should be included as well
// - flag to delete files after submit?
export interface AttachInitResponse {
  challengeName: string;
  challengeId: string;
  candidateId: string;
  ablyToken: string;
  ablyChannel: string;
  status: "started" | "pending";
  files: File[];
}

// Alert types supported by web UI. Defaults to "info".
export type AlertType = "info" | "success" | "warning" | "danger";
